rm(list = ls())
require("ggplot2")
library(scales)
library(dplyr)

timetomins = function(time)
{
  hr = as.numeric(substr(time,12,13))
  min = as.numeric(substr(time,15,16))
  return ((hr*60) + (min+1))
}
getSmp = function(data)
{
  retval = unlist(rep(NA,length(data[,1])))
  tm = as.character(data[,1])
  smpval = as.numeric(data[,3])
  for(x in 1 : length(tm))
  {
    start = max(1,(x-15))
    end = min(1440,(x+15))
    tmp = smpval[start:end]
    tmp = tmp[complete.cases(tmp)]
    if(length(tmp))
    {
      tmp = round(mean(tmp),2)
      retval[x] = tmp
    }
  }
  return(retval)
}

path1 = "/home/admin/Dropbox/Cleantechsolar/1min/[724]/2019"
path2 = "/home/admin/Dropbox/CS Data/Clear Sky SGNew/Txt Values"

yrs1 = dir(path1)
yrs2 = dir(path2)

pathwrite = "/home/admin/Dropbox/CS Data/Clear Sky SGNew Compare"
if(!file.exists(pathwrite))
  dir.create(pathwrite)
pathwrite = "/home/admin/Dropbox/CS Data/Clear Sky SGNew Compare/2019"
if(!file.exists(pathwrite))
  dir.create(pathwrite)
for(x in 1 : length(yrs1))
{
  mon1 = paste(path1,yrs1[x],sep="/")
  mon2 = paste(path2,yrs2[x],"1-min",sep="/")
  pathwritemon = paste(pathwrite,yrs1[x],sep="/")
  if(!file.exists(pathwritemon))
    dir.create(pathwritemon)
  days1 = dir(mon1)
  days2 = dir(mon2)
  for(y in 1 :length(days1))
  {
    path1f = paste(mon1,days1[y],sep="/")
    path2f = paste(mon2,days2[y],sep="/")
    pathwritef = paste(pathwritemon,gsub("\\[724\\] ","",days1[y]),sep="/")
    pathwriteg = gsub("txt","pdf",pathwritef)
    data1 = read.table(path1f,sep="\t",header=T,stringsAsFactors=F)
    data2 = read.table(path2f,sep="\t",header=T,stringsAsFactors=F)
    date = as.character(data1[,1])
    idx = c()
    for( x in 1 : length(date))
    {
      idx[x] = timetomins(date[x])
    }
    csirr = as.numeric(data2[idx,2])
    smpirr = getSmp(data1)
    df = data.frame(Date=date,CS=csirr,SMP=smpirr,stringsAsFactors=F)
    smptot = round(sum(as.numeric(smpirr))/60000,2)
    cstot = round(sum(as.numeric(csirr))/60000,2)
    write.table(df,file=pathwritef,row.names=F,col.names=T,append=F,sep="\t")
    titlesettings <- theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                  plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))
    xax = c(1:length(date))
    df = data.frame(Date=xax, CS=csirr, SMP=smpirr,stringsAsFactors=F)
    lbl = c("00:00","02:00","04:00","06:00","08:00","10:00","12:00","14:00","16:00","18:00","20:00","22:00","23:59")
    seq1 = seq(from=1,to=length(xax),by=120)
p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_line(data=df, aes(x=Date, y =CS), size = 1.5, color="blue")
p1 <- p1 + geom_line(data=df, aes(x=Date, y =SMP), size = 1.5, color="red")
p1 <- p1 +  ylab("Irradiance [W/m^2]") + xlab("") + coord_cartesian(ylim = c(0,1200))
p1 <- p1 + scale_x_continuous(breaks = seq1,labels = lbl[1:length(seq1)]) 
p1 <- p1 + scale_y_continuous(breaks=seq(0, 1200, 100))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle(paste('CS vs SMP - SG-724', substr(as.character(date[1]),1,10)))
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p1 <- p1 + annotate('text', label = paste("CS Irr [kWh/m^2]: ",cstot,sep=""), y = 700 , x = 301, colour = "blue")
p1 <- p1 + annotate('text', label = paste("SMP Irr [kWh/m^2]: ",smptot,sep=""), y = 600 , x = 301, colour = "red")

ggsave(pathwriteg, p1, width = 11, height=8)
  }
}
