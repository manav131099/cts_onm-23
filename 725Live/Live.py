from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[SG-005S]/'
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=8,microsecond=0)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=0)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
cols=[]
cols_2=[]
cols_3=[]
cols_4=[]
for i in range(600):
    cols.append(i+1)
    cols_2.append(10*i+2)
    cols_3.append(100*i+3)
    cols_4.append(1000*i+4)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
flag=1
starttime=time.time()
cols2=['Time Stamp','AvgSMP10','AvgGsi00','AvgGmod','AvgTamb','AvgHamb','AvgTmod','AvgWindS','AvgWindD','Act_Pwr-Tot_F2','Act_E-Del_F2','Act_E-Recv_F2','Act_Pwr-Tot_F3-5','Act_E-Del_F3-5','Act_E-Recv_F3-5','Act_Pwr-Tot_F7A','Act_E-Del_F7A','Act_E-Recv_F7A','Act_Pwr-Tot_F7B','Act_E-Del_F7B','Act_E-Recv_F7B','Act_Pwr-Tot_F7G','Act_E-Del_F7G','Act_E-Recv_F7G']
while(1):
    if(flag==1):
        cur=currtime.strftime("%Y-%m-%d_%H-%M")
    else:
        curr=datetime.datetime.now(tz)
        currtime=curr.replace(tzinfo=None)
        cur=currtime.strftime("%Y-%m-%d_%H-%M")
    print('Now adding for',currtime)
    flag2=1
    j=0
    while(j<5 and flag2!=0):
        j=j+1
        files=ftp.nlst()        
        for i in files:
            if(re.search(cur,i)):
                req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                try:
                    with urllib.request.urlopen(req) as response:
                        s = response.read()
                    df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                    a=df.loc[df[1] == 724]
                    a.columns=cols
                    a2=df.loc[df[1] == 725]
                    a2.columns=cols_2
                    a3=df.loc[df[1] == 726]
                    a3.columns=cols_3
                    a4=df.loc[df[1] == 727]
                    a4.columns=cols_4
                    if(flag==1):
                        b=a[[2,3,4,5,6,7,8,9,10,24,39,40]]
                        b2=a2[[152,302,312]]
                        b3=a3[[1503,3003,3103,6203,7703,7803]]
                        b4=a4[[15004,30004,31004]]
                        res=pd.concat([b, b2])
                        res=pd.concat([res, b3])
                        res=pd.concat([res, b4])
                        res[2]=res[2].values[0]
                        res=res.groupby([2], as_index=False).sum()
                        flag=0
                    else:
                        b=a[[2,3,4,5,6,7,8,9,10,24,39,40]]
                        b2=a2[[152,302,312]]
                        b3=a3[[1503,3003,3103,6203,7703,7803]]
                        b4=a4[[15004,30004,31004]]
                        res=pd.concat([b, b2])
                        res=pd.concat([res, b3])
                        res=pd.concat([res, b4])
                        res[2]=res[2].values[0]
                        res=res.groupby([2], as_index=False).sum()
                        if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt')):
                            df= pd.read_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt',sep='\t')
                            df2= pd.DataFrame( np.concatenate( (df.values, res.values), axis=0 ) )
                            df2.columns = cols2
                        std1=df2['Act_E-Del_F2'].std()
                        std2=df2['Act_E-Del_F3-5'].std()
                        std3=df2['Act_E-Del_F7A'].std()
                        std4=df2['Act_E-Del_F7B'].std()
                        std5=df2['Act_E-Del_F7G'].std()
                        if(std1>13000):
                            res.loc[:, 39] = 'NaN' 
                        if(std2>13000):
                            res.loc[:, 302] = 'NaN' 
                        if(std3>13000):
                            res.loc[:, 3003] = 'NaN' 
                        if(std4>13000):
                            res.loc[:, 7703] = 'NaN'
                        if(std5>13000):
                            res.loc[:, 30004] = 'NaN'
                        res.loc[res[39] == 0, 39] = 'NaN'
                        res.loc[res[302] == 0, 302] = 'NaN'
                        res.loc[res[3003] == 0, 3003] = 'NaN'
                        res.loc[res[7703] == 0, 7703] = 'NaN'
                        res.loc[res[30004] == 0, 30004] = 'NaN'
                        res.loc[res[39] < 0, 39] = 'NaN'
                        res.loc[res[302] < 0, 302] = 'NaN'
                        res.loc[res[3003] < 0, 3003] = 'NaN'
                        res.loc[res[7703] < 0, 7703] = 'NaN'
                        res.loc[res[30004] < 0, 30004] = 'NaN'
                    #Taking care of Power
                    res.loc[(res[3] < 0), 3] = 0
                    if(currtime.hour>19):
                        res.loc[(res[24] > 50) | (res[24] < -2), 24] = 'NaN'
                        res.loc[(res[152] > 50) | (res[152] < -2), 152] = 'NaN'
                        res.loc[(res[1503] > 50) | (res[1503] < -2), 1503] = 'NaN'
                        res.loc[(res[6203] > 50) | (res[6203] < -2), 6203] = 'NaN'
                        res.loc[(res[15004] > 50) | (res[15004] < -2), 15004] = 'NaN'
                    res.loc[(res[24] > 2000) | (res[24] < -2), 24] = 'NaN'
                    res.loc[(res[152] > 2000) | (res[152] < -2), 152] = 'NaN'
                    res.loc[(res[1503] > 2000) | (res[1503] < -2), 1503] = 'NaN'
                    res.loc[(res[6203] > 2000) | (res[6203] < -2), 6203] = 'NaN'
                    res.loc[(res[15004] > 2000) | (res[15004] < -2), 15004] = 'NaN'
                    res.columns=cols2
                    chkdir(path+cur[0:4]+'/'+cur[0:7])
                    if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt')):
                        res.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                    else:
                        res.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt',header=True,sep='\t',index=False)
                    flag2=0
                except Exception as e:
                    flag2=0
                    print(e)
                    print("Failed for",currtime)                  
        if(flag2!=0 and j<5):
            print('sleeping for 10 seconds!')            
            time.sleep(10)
    if(flag2==1):
        print('File not there!',cur)
    print('sleeping for a minute!')
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        