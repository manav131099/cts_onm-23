source('/home/admin/CODE/common/aggregate.R')

registerMeterList("SG-001X",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 11 #column for Yld-2
aggColTemplate[9] = 12 #column for PR-1
aggColTemplate[10] = 13 #column for PR-2
aggColTemplate[11] = 9 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-001X","",aggNameTemplate,aggColTemplate)
