rm(list = ls())
errHandle = file('/home/admin/Logs/LogsKH003XHistory.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/KH003Digest/FTPKH003Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')

FIREERRATA = c(1,1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}
FIRETWILIONA =c(0,0,0)
THRESHOLDCOUNT = 10
FIRETWILIOBELOW = c(0,0,0)
GLOBALPATHCKL = GLOBALPATHCKX = " "
checkErrata = function(row,ts,no)
{
	{
  if(no == 1)
	  no2 = 'SLX'
	else if(no == 2)
	  no2 = 'CKX'
	else if (no==3)
		no2 = 'CKL'
	}
  if(ts < 540 || ts > 1020 || no ==3)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		FIRETWILIONA[no] <<- FIRETWILIONA[no] + 1
	}
	else
	{
		if(as.numeric(row[2]) < LTCUTOFF)
			FIRETWILIOBELOW[no] <<- FIRETWILIOBELOW[no] + 1
		else
		{
			FIRETWILIONA[no] <<- 0
			FIRETWILIOBELOW[no] <<- 0
		}
	}
	}
	if((FIRETWILIONA[no] > THRESHOLDCOUNT) || (FIRETWILIOBELOW[no] > THRESHOLDCOUNT))
	{
		row[2] = as.numeric(row[2])/1000
		FIREERRATA[no] <<- 0
		subj = paste('KH-003X',no2,'down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'\nTimestamp:',as.character(row[1]))
		body = paste(body,'\n\nReal Power Tot kW reading:',as.character(round(as.numeric(row[2]),3)))
		{
			if(FIRETWILIONA[no]>THRESHOLDCOUNT)
				body = paste(body,'\n\nMore than 10 continuous readings are NA')
			else
				body = paste(body,'\n\nMore than 10 continuous readings are below 0.001 kW')
		}
		sending = try(send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("KH-003X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F),silent = T)
		if(class(sending)=='try-error')
		{
			print('Error sending mail... trying again in 60s')
			Sys.sleep(60)
			sending = try(send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("KH-003X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F),silent = T)
		}
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "KH-003X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("KH-003X","TwilioAlert",as.character(row[1]))
		FIRETWILIONA[no] <<-0
		FIRETWILIOBELOW[no] <<- 0
	}
}
doextratinkering = function(path1,path2)
{
	print(paste('Inside tinkering'))
	print(paste('path1 is',path1))
	print(paste('path2 is',path2))
	if(file.exists(path1) && file.exists(path2))
	{
	dataread1 = read.table(path1,header = T,sep="\t",stringsAsFactors=F)
	dataread2 = read.table(path2,header = T,sep="\t",stringsAsFactors=F)
	idxmtch= match(as.character(dataread2[,1]),as.character(dataread1[,1]))
	idxmtch2= match(as.character(dataread1[,1]),as.character(dataread2[,1]))
	idxmtch = idxmtch[complete.cases(idxmtch)]
	idxmtch2 = idxmtch2[complete.cases(idxmtch2)]
	col1 = as.numeric(dataread1[idxmtch,15])
	col2 = as.numeric(dataread2[idxmtch2,15])
	if(length(col1) == length(col2))
	{
	artificial = col1 + col2
	colnm = colnames(dataread1)
	if(!is.finite(match("Artificial",colnm)))
	{
		colnm = c(colnm,"Artificial")
	}
	if(length(artificial)<nrow(dataread1))
	{
		artificial[(length(artificial)+1):nrow(dataread1)] = NA
	}
	if(length(artificial) > nrow(dataread1))
	{
		artificial = artificial[1:nrow(dataread1)]
	}
	dataread1[,length(colnm)] = as.character(artificial)
	print('Done')
	colnames(dataread1) = colnm
	write.table(dataread1,file=path1,col.names =T,row.names = F,append = F,sep ="\t")
	print('done writing')
	}
	}
}

stitchFile = function(path,days,pathwrite,erratacheck,colnames,rowtemp)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
  allseqs = seq(from = 1, to = length(tmstmps),by=2)
	tmstmps2 = tmstmps[allseqs]
	tmstmps2 = unlist(strsplit(tmstmps2,"-"))
	allseqa = seq(from = 1, to = length(tmstmps2),by=3)
	tmstmps2 = paste(tmstmps2[(allseqa+2)],tmstmps2[allseqa],tmstmps2[(allseqa+1)],sep="-")
	tmstmps = paste(tmstmps2,tmstmps[(allseqs+1)])
	dataread[,1] = tmstmps
	currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  temp = unlist(strsplit(days[x],"_"))
	print(paste('temp is',temp[1]))
  {
	if(temp[1] == "SLX" || temp[1] == "SLK")
		temp = 1
	else if(temp[1] == "CKX")
		temp = 2
	else if(temp[1] == "CKL")
		temp = 3
	}
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
		rowtemp3 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		{
		if(temp==1)
		  temp2 = 'SLX'
		else if(temp == 2)
		  temp2 = 'CKX'
		else if(temp == 3)
			{
			temp2 = 'CKL'
			 rowtemp2[(length(rowtemp2)+1)] = NA
			 rowtemp3[(length(rowtemp3)+1)] = NA
			 }
    }
		pathwritefinal = paste(pathwritemon,temp2,sep="/")
    checkdir(pathwritefinal)
    mtno = temp
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,temp,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
		idxvals = idxvals[complete.cases(idxvals)]
    rowtemp2[idxvals] = dataread[y,]
		{
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
			{
			if(temp==3)
			colnames(df) = c(colnames,"Artificial")
			else
      colnames(df) = colnames
			}
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp3
        }
			FIREERRATA <<- c(1,1,1)
			FIRETWILIONA <<- c(0,0,0)
			FIRETWILIOBELOW <<- c(0,0,0)
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,15]))
	checkErrata(pass,idxts,mtno)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
	{
	if(temp2 == 'CKL')
		GLOBALPATHCKL <<- pathtowrite
	else if(temp2 == 'CKX')
		GLOBALPATHCKX <<- pathtowrite
  }
	}
	recordTimeMaster("KH-003X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX KH003X Dump'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[KH-003X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'KH003X.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
idxtostartwith = days[grepl('CKX',days)]
idxtostartwith = match(idxtostartwith[1],days)

lastrecordeddate = c('','','')
idxtostart = c(1,1,1)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[KH-003X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1],lastrecordeddate[2],lastrecordeddate[3])
		idxtostart[1] = match(lastrecordeddate[1],days)
		idxtostart[2] = match(lastrecordeddate[2],days)
		idxtostart[3] = match(lastrecordeddate[3],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
		print(paste('Date read is',lastrecordeddate[2],'and idxtostart is',idxtostart[2]))
		print(paste('Date read is',lastrecordeddate[3],'and idxtostart is',idxtostart[3]))
	}
}
checkdir(pathwrite)
colnamesa = colnames(read.csv(paste(path,days[idxtostart[1]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesb = colnames(read.csv(paste(path,days[idxtostart[2]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesc = colnames(read.csv(paste(path,days[idxtostart[3]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesa = colnamesa[-c(1,2)]
colnamesb = colnamesb[-c(1,2)]
colnamesc = colnamesc[-c(1,2)]
rowtempa = rep(NA,(length(colnamesa)))
rowtempb = rep(NA,(length(colnamesb)))
rowtempc = rep(NA,(length(colnamesc)))
x=1
stnname =  "[KH-003X-M"
days1 = days[grepl('SLX',days)]
days2 = days[grepl('CKX',days)]
days3 = days[grepl('CKL',days)]

{
	if(idxtostart[1] <= length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0,colnamesa,rowtempa)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if(idxtostart[2] <= length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in idxtostart[2] : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0,colnamesb,rowtempb)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 Done')
	}
	if(idxtostart[3] <= length(days3))
	{
		print(paste('idxtostart[3] is',idxtostart[3],'while length of days3 is',length(days3)))
		for(x in idxtostart[3] : length(days3))
		{
 		 stitchFile(path,days3[x],pathwrite,0,colnamesc,rowtempc)
		}
		lastrecordeddate[3] = as.character(days3[x])
		print('Meter 3 Done')
	}
	
	else if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))) && (!(idxtostart[3] < length(days3))))
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		if(grepl('SLX',daysnew[x])){
		colstopass = colnamesa
		rowstopass = rowtempa
		print('passing colnamesa')
		}
		if(grepl('CKX',daysnew[x])){
		print('passing colnamesb')
		colstopass = colnamesb
		rowstopass = rowtempb
		}
		if(grepl('CKL',daysnew[x])){
		print('passing colnamesc')
		colstopass = colnamesc
		rowstopass = rowtempc
		}
		stitchFile(path,daysnew[x],pathwrite,1,colstopass,rowstopass)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew[grepl('SLX',daysnew)]
	daysnew2 = daysnew[grepl('CKX',daysnew)]
	daysnew3 = daysnew[grepl('CKL',daysnew)]
	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	if(length(daysnew2) < 1)
		daysnew2 = lastrecordeddate[2]
	if(length(daysnew3) < 1)
		daysnew3 = lastrecordeddate[3]
	write(c(as.character(daysnew1[length(daysnew1)]),as.character(daysnew2[length(daysnew2)]),as.character(daysnew3[length(daysnew3)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
	doextratinkering(GLOBALPATHCKL,GLOBALPATHCKX)
gc()
}
print('Out of loop')
sink()
