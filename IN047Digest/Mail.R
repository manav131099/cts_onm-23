rm(list=ls())
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-047T]"')
source('/home/admin/CODE/IN047Digest/2G3GFunctions.R')
require('mailR')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
errHandle = file('/home/admin/Logs/LogsIN047Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}
daysAlive = 0
DOB = "15-08-2018"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-047T","m")
pwd = 'CTS&*(789'

sendMail= function(path)
{
	dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	filenams = paste(currday,".txt",sep="")
	body = "Site Name: Fortuna Engineering D-16\n"
	body = paste(body,"\nLocation: Nashik, India\n",sep="")
	body = paste(body,"\nO&M Code: IN-047\n",sep="")
	body = paste(body,"\nSystem Size: 180.01 kWp\n",sep="")
	body = paste(body,"\nNumber of Energy Meters: 1\n",sep="")
	body = paste(body,"\nModule Brand / Model / Nos: REC / REC-PE72- 310 and 315 / 580\n",sep="")
	body = paste(body,"\nInverter Brand / Model / Nos: Huawei / Sun2000 36-KTL / 4\n",sep="")
	body = paste(body,"\nSite COD: 2018-08-15\n",sep="")
	body = paste(body,"\nSystem age [days]: ",(15+daysAlive),sep="")
	body = paste(body,"\n\nSystem age [years]: ",round((15+daysAlive)/365,2),sep="")
	bodyac = body
	body = "\n\n__________________________________________________\n\n"
	body = paste(body,currday,sep="")
	body = paste(body,"\n\n__________________________________________________\n\n",sep="")
	body = paste(body,"DA [%]: ",as.character(dataread[1,2]),"\n\n",sep="")
	body = paste(body,"EAC method-1 (Pac) [kWh]: ",as.character(dataread[1,3]),"\n\n",sep="")
	body = paste(body,"EAC method-2 (Eac) [kWh]: ",as.character(dataread[1,4]),"\n\n",sep="")
	body = paste(body,"Total Irradiance [kWh/m^2]: ",as.character(dataread[1,9]),"\n\n",sep="")
	body = paste(body,"Yield-1 [kWh/kWp]: ",as.character(dataread[1,5]),"\n\n",sep="")
	body = paste(body,"Yield-2 [kWh/kWp]: ",as.character(dataread[1,6]),"\n\n",sep="")
	body = paste(body,"PR-1 [%]: ",as.character(dataread[1,10]),"\n\n",sep="")
	body = paste(body,"PR-2 [%]: ",as.character(dataread[1,11]),"\n\n",sep="")
	body = paste(body,"Last recorded value [kWh]: ",as.character(dataread[1,7]),"\n\n",sep="")
	body = paste(body,"Last recorded timestamp: ",as.character(dataread[1,8]),"\n\n",sep="")
	body = paste(body,"Station DOB: ",as.character(DOB),"\n\n",sep="")
	body = paste(body,"Days alive: ",as.character(daysAlive),"\n\n",sep="")
  body = paste(bodyac,body,sep="")
	body = gsub("\n ","\n",body)
	send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-047T] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = path,
            file.names = filenams, # optional paramete
            debug = F)
 recordTimeMaster("IN-047T","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-047T]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-047T]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-047T]'

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-047T"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"] ",lastdatemail,".txt",sep="")
ENDCALL=0
for(x in 1 : length(years))
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	path3Gyr = paste(path3G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	checkdir(path3Gyr)
	months = dir(pathyr)
	for(y in 1 : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		path3Gfinal = paste(path3Gyr,"/",months[y],".txt",sep = "")
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		days = dir(pathmon)
		if(length(days) > 1)
		{
      if(ENDCALL==1)
			 break
		for(z in 1 : length(days))
		{
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon,days[z],sep="/")
			secondGenData(pathfinal,path2Gfinal)
			thirdGenData(path2Gfinal,path3Gfinal)
			daysAlive = daysAlive+1
			if(days[z] == stopDate)
			{
				ENDCALL = 1
			}
		}
		}
		if(ENDCALL == 1)
		break
	}
	if(ENDCALL == 1)
		break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
while(1)
{
	recipients = getRecipients("IN-047T","m")
	recordTimeMaster("IN-047T","Bot")
	years = dir(path)
	noyrs = length(years)
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		path3Gyr = paste(path3G,years[x],sep="/")
		checkdir(path2Gyr)
		checkdir(path3Gyr)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			path3Gfinal = paste(path3Gyr,"/",mons[y],".txt",sep="")
			days = dir(pathmon)
			chkcopydays = days[grepl('Copy',days)]
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are'),idxflse)
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			if(y > startmn)
			{
				z = prevz = 1
			}
			nodays = length(days) 
			if(prevz <= nodays)
			{
			for(z in prevz : nodays)
			{
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				pathdays = paste(pathmon,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal)
				thirdGenData(path2Gfinal,path3Gfinal)
				print('Sending mail')
				daysAlive = daysAlive+1
				sendMail(path2Gfinal)
			}
			}
		}
	}
	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
