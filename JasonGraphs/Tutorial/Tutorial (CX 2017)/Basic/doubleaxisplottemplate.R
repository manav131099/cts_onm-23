
#######
pdf("replace by the directory to save your file",width=7.92,height=5)
#######################


#the value for first curve
yaxis <- ratio1
#the value for second curve
yaxis2 <- ratio2

#the x-axis
xaxis <- c(1:length(yaxis))

#the place to put label on xaxis
xaxis2 <- seq(-4,(length(yaxis)),30.5)


par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, ratio1, pch=4, axes=FALSE, ylim=c(0,200), xlab="", ylab="", 
     type="l",col="blue", main=" ")
axis(2, ylim=c(0,200),at = seq(0,200,25),col="black",col.axis="black",las=1)
#add the label of yaxis
mtext(expression(bold("GmodE/Gsi200 Ratio [%]")),side=2,line=2.5,cex=1,las =3)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, ratio2, pch=3,  xlab="", ylab="", ylim=c(50,250), 
     axes=FALSE, type="l", col="orange")

axis(4, ylim=c(50,250), at = seq(50,250,25),col="black",col.axis="black",las=1)
#add the label of the other yaxis
text(length(xaxis)*1.14,150,expression(paste(bold("GmodW/Gsi00 Ratio [%]"))),xpd=NA,srt = -90, cex = 1)

#add anotation
text1 <- paste0("Average GmodE/Gsi00 = +",format(round(mean(ratio1)-100,1),nsmall = 1))
text2 <- paste("Average GmodW/Gsi00 = ",format(round(mean(ratio2)-100,1),nsmall = 1))
text(length(xaxis)/2,225,paste0(text1,"%"),xpd=NA,cex = 1,col="blue")
text(length(xaxis)/2,75,paste0(text2,"%"),xpd=NA,cex = 1,col="orange")


#add title
mtext(expression(paste(bold("GmodE/Gsi00 & GmodW/Gsi00 Ratios"))),side=3,col="black",line=1.75,cex=1.2)  
mtext(expression(paste(bold("from 06-06-2016 to 05-05-2017"))),side=3,col="black",line=0.75,cex=1.2)  

#add label on xaxis
axis(1,at = xaxis2,cex = 1.1,labels = c("Jun", "Jul", "Aug", "Sep","Oct","Nov","Dec","Jan","Feb","Mar","Apr","May"))
legend(length(yaxis)*0.74,258,
       c("GmodE/Gsi00","GmodW/Gsi00"),# places a legend at the appropriate place c(???Health???,???Defense???), # puts text in the legend
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("blue","orange")) # gives the legend lines the correct color and width


par(new=TRUE)

dev.off()

