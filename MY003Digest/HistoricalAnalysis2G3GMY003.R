system('rm -R "/home/admin/Dropbox/Second Gen/[MY-003X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[MY-003X]"')
system('rm -R "/home/admin/Dropbox/Fourth_Gen/[MY-003X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/MY003Digest/MY003MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[MY-003X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[MY-003X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[MY-003X]'
checkdir(writepath3G)
DAYSALIVE = 0
years = dir(path)
stnnickName2 = "MY-003X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
writepath4G = "/home/admin/Dropbox/Fourth_Gen/[MY-003X]"
checkdir(writepath4G)
stopDate = paste("[",stnnickName2,"-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0
timeStampAzureFile = "/home/admin/Start/MY003XA4G.txt"

timeStampAzure = NA
if(file.exists(timeStampAzureFile)){
	timeStampAzure = readLines(timeStampAzureFile)
	tmAc = as.POSIXct(strptime(as.character(timeStampAzure), "%Y-%m-%d %H:%M:%S"))
	tmAc = tmAc - 1
	timeStampAzure = as.character(tmAc)
}

for(x in 1 : length(years)){
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months)){
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[MY-003X] ",months[y],".txt",sep="")
    stations = dir(pathmonths)
		#if(length(stations) > 2)
		#{
		#  stations = c(stations[2],stations[3],stations[1])
	  #		print('rewordring')
    #			print(stations)
	  #}
    pathdays = paste(pathmonths,stations[1],sep="/")
    pathdays2 = paste(pathmonths,stations[2],sep="/")
    writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
    writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
    checkdir(writepath2Gdays)
    checkdir(writepath2Gdays2)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    days2 = dir(pathdays2)
		if(length(days) != length(days2)){
			minlen = min(length(days),length(days2))
			days = tail(days,n=minlen)
			days2 = tail(days2,n=minlen)
		}
		days3 = NULL
		if(length(stations) > 2){
      pathdays3 = paste(pathmonths,stations[3],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
		  days3 = dir(pathdays3)
      if(length(days3) == length(days)){
			checkdir(writepath2Gdays3)
		  }
		}
    for(t in 1 : length(days)){
      if(ENDCALL==1)
				break
				{
          if(x == 1 && y == 1 && t == 1){
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }else if(x == length(years) && y == length(months) && t == length(days)){
            next
          }
        }
      DAYSALIVE = DAYSALIVE + 1
      writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
      writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
      readpath = paste(pathdays,days[t],sep="/")
      readpath2 = paste(pathdays2,days2[t],sep="/")
      METERCALLED <<- 1 #correct dont change
      df1 = secondGenData(readpath,writepath2Gfinal)
      print(days[t])
      METERCALLED <<- 2 # correct dont change
      df2 = secondGenData(readpath2,writepath2Gfinal2)
      print(days2[t])
				
			if(length(stations)>2 && (length(days3)==length(days))){
        writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
        readpath3 = paste(pathdays3,days3[t],sep="/")
				METERCALLED <<- 3
				df3 = secondGenData(readpath3,writepath2Gfinal3)
				print(days3[t])
        thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath3Gfinal,timeStampAzure)
			}else{
        thirdGenData(writepath2Gfinal,writepath2Gfinal2,NULL,writepath3Gfinal,timeStampAzure)
			}
				
			print(writepath3Gfinal)
			if(days[t] == stopDate){
					ENDCALL = 1
			}
    }
			if(ENDCALL == 1)
				break
  }
	if(ENDCALL == 1)
		break
}
print('Historical Analysis Done')
