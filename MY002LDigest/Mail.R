
rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsMY002LMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/MY002LDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(3,1,2)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/MY002LDigest/aggregateInfo.R')

METERNICKNAMES = c("WMS","MFM-1","MFM-2")

METERACNAMES = c("WMS_1_Reference Cell","MFM_1_PV Meter-01","MFM_2_PV Meter-02")

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

DOB = "02-08-2018"
DOB2="2018-08-02"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
# recipients =getRecipients("MY-002L","m")
recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
  if(length(days!=0))
  {
    seq = seq(from=1,to=length(days)*6,by=6)
    days = unlist(strsplit(days,"-"))
    days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
    return(days)
  }
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/Gen 1 Data/[MY-002L]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Denis Group, Mafipro",sep="")
  body = paste(body,"\n\nLocation: Taiping, Perak, Malaysia - 34600")
  body = paste(body,"\n\nO&M Code: MY-002")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body,"\n\nNumber of Energy Meters: 2")
  body = paste(body,"\n\nModule Brand / Model / Nos: JINKO / 330 / 1275")
  body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / Solid-Q50 / 8")
  body = paste(body,"\n\nSite COD: 2018-08-02")
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(Cur-DOB2))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(Cur-DOB2))/365,1)))
  body = paste(body,"\n\n")
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  globMtYld=c()
  idxglobMtYld=1
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall))
  {
    type = 0
    meteridx = t
    print("Printing Pathall")
    print(pathall[t])
    print("-----------------")
    if(grepl("WMS_1_Reference",pathall[t])) 
      type = 1
    else if(grepl("MFM_1",pathall[t]) | grepl("MFM_2", pathall[t]))
      type=1
    else
      type = 0
    if( t > (1+NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"INVERTER_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+2],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }
    else
    {
      filenams[t] = paste(currday,"-",'MFM',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    print("Printing pathall-2")
    print(pathall[t])
    if(type == 1)
    {
      body = paste(body,"\n\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        
        if(grepl("MFM",pathall[t]))
        {
          if(grepl("MFM_1",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[1],"\n\n")
            body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
            body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
            TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
            yld1 = dataread[1,3]/316.8
            yld2 = dataread[1,4]/316.8
            print("Printing yields")
            print(yld1)
            print(yld2)
            body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(dataread[1,3]/316.8,2), nsmall = 2)),"\n\n")
            body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(dataread[1,4]/316.8,2), nsmall = 2)),"\n\n")
            globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
            idxglobMtYld = idxglobMtYld + 1
            body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round(dataread[1,3]*100/316.8/GHI,1), nsmall = 1)),"\n\n")
            body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round(dataread[1,4]*100/316.8/GHI,1), nsmall = 1)),"\n\n")
            body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
            body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
            body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
            body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
          }else if(grepl("MFM_2",pathall[t])){
            body = paste(body,"System Size [kWp]:",INSTCAPM[2],"\n\n")
            
            body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
            body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
            TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
            yld1 = round(dataread[1,3]/103.95,2)
            yld2 = round(dataread[1,4]/103.95,2)
            print("Printing yields")
            print(yld1)
            print(yld2)
            body = paste(body,"Yield-1 [kWh/kWp]:",yld1,"\n\n")
            body = paste(body,"Yield-2 [kWh/kWp]:",yld2, "\n\n")
            globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
            idxglobMtYld = idxglobMtYld + 1
            body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round(yld1*100/GHI,1), nsmall = 1)),"\n\n")
            body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round(yld2*100/GHI,1), nsmall = 1)),"\n\n")
            body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
            body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
            body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
            body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
          }
        }
        else
        {
          GHI = dataread[1,3] 
          body = paste(body,"GHI [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2)),"\n\n")
          body = paste(body,"Avg Tmod [C]:",as.character(format(round(dataread[1,5],1), nsmall = 1)),"\n\n")
          body = paste(body,"Avg Tmod solar hours [C]:",as.character(format(round(dataread[1,7],1), nsmall = 1)))
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx)] = as.numeric(dataread[1,7])
  }
  #body = paste(body,"\n\n________________________________________________\n\n")
  #body = paste(body,currday,"Inverters")
  #body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  print(MYLD)
  print(MYLDCPY)
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),1)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  #for(t in 1 : noInverters)
  #{
  #  body = paste(body,"\n\n Yield ",acname[(t+1+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
  #  if( addwarn && is.finite(covar) && covar > 5 &&
  #      (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
  #  {
  #    body = paste(body,">>> Inverter outside range")
  #  }
  #}
  #body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  #for(t in 1 : noInverters)
  #{
  #  body = paste(body,"\n\n Inverter Availability ",acname[(t+1+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
  #}
  
 # for(t in 1 : NOMETERS)
#  {
#    bodyac = paste(bodyac,"Yield",acname[(t+1)],"[kWh/kWp]:",as.character(format(round(globMtYld[t],2), nsmall = 2,)),"\n\n")
#  }
bodyac = paste(bodyac,"System Full Generation [kWh]:",TOTALGENCALC)	
bodyac = paste(bodyac,"\n\nSystem Full Yield [kWh/kWp]:", (format(round(TOTALGENCALC/INSTCAP, 2))))	
bodyac = paste(bodyac,"\n\nSystem Full PR [%]:", format(round(TOTALGENCALC/INSTCAP/GHI*100, 1)), "\n\n")
	for(t in 1 : NOMETERS)	
{	
bodyac = paste(bodyac,"Yield",acname[(t+1)],"[kWh/kWp]:",as.character(format(round(globMtYld[t],2), nsmall = 2,)),"\n\n")	
}
  sdm =round(sdp(globMtYld),3)
  covarm = round(sdm*100/mean(globMtYld),1)
  bodyac = paste(bodyac,"Stdev/COV Yields:",as.character(format(round(sdm,1), nsmall = 1)),"/",as.character(format(round(covarm,1), nsmall = 1)),"[%]")
  
  
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  #firecond = lastMailDate(paste('/home/anusha/Test/MasterMail/MY-002L_Mail.txt',sep=""))
  #if (currday == firecond)
  #  return()
  
  graph_command=paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R", "MY-002", 75.7, 0.008, "2018-08-02", currday, sep=" ")
  system(graph_command)
  graph_path=paste('/home/admin/Graphs/Graph_Output/MY-002/[MY-002] Graph ',currday,' - PR Evolution.pdf',sep="")
  graph_extract_path=paste('/home/admin/Graphs/Graph_Extract/MY-002/[MY-002] Graph ',currday,' - PR Evolution.txt',sep="")

  graph_command1=paste('python3 /home/admin/CODE/MY002LDigest/Meter_CoV_Graph.py "MY-002"',currday,3,sep=" ")
  system(graph_command1)
  graph_path2=paste('/home/admin/Graphs/Graph_Output/MY-002/[MY-002] Graph ',currday,' - Meter CoV.pdf',sep="")
  graph_extract_path2=paste('/home/admin/Graphs/Graph_Extract/MY-002/[MY-002] Graph ',currday,' - Meter CoV.txt',sep="")

  pathall=c(graph_path, graph_extract_path,graph_path2,graph_extract_path2, pathall)

  
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [MY-002L] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            debug = F)
  recordTimeMaster("MY-002L","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[MY-002L]"
path2G = '/home/admin/Dropbox/Second Gen/[MY-002L]'
path3G = '/home/admin/Dropbox/Third Gen/[MY-002L]'
path4G = '/home/admin/Dropbox/Fourth_Gen/[MY-002L]'


if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "MY-002L"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-MFM2-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      print("Printing stations")
      print(stns[t])
      print("-------------------")
      if(grepl("MFM_1", stns[t]) | grepl("MFM_2", stns[t]))
        type = 0
      if(grepl("WMS_1_Reference",stns[t]))
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      
      
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  # recipients = getRecipients("MY-002L","m")
  recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
  recordTimeMaster("MY-002L","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 3)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(grepl("MFM_1",stns[t]) | grepl("MFM_2", stns[t]))
          type = 0
        if(grepl("WMS_1_Reference",stns[t]))
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(t==1)
          referenceDays <<- extractDaysOnly(days)
        else
          days = analyseDays(days,referenceDays)
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()