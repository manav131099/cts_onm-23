rm(list = ls())
errHandle = file('/home/admin/Logs/LogsCentral.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
checkdir = function(path)
{
	if(!file.exists(path))
		dir.create(path)
}

fetchCols = function(stnId)
{
	colreturn = c(NA,NA)
	{
		if(grepl("SG-001",stnId))
			colreturn = c(7,8)
		else if(grepl("SG-002",stnId))
			colreturn = c(7,8)
		else if(grepl("SG-003",stnId))
			colreturn = c(7,8)
		else if(grepl("SG-004",stnId))
			colreturn = c(7,8)
		else if(grepl("SG-006",stnId))
			colreturn = c(7,8)
		else if(grepl("SG-005",stnId))
			colreturn = c(45,44)
		else if(grepl("SG-725",stnId))
			colreturn = c(11,12)
		else if(grepl("SG-726",stnId))
			colreturn = c(11,12)
	}
}

path = "/home/admin/Dropbox/Second Gen"

stns = dir(path)

writePath = "/home/admin/Dropbox/Centralize"

stns = dir(path)

stns = stns[grepl("SG",stns)]
while(1)
{
for( x in 1 : length(stns))
{
	stnId = gsub("\\[","",stns[x])
	stnId = gsub("\\]","",stnId)
	print(stnId)
	pathstn = paste(path,stns[x],sep="/")
	years = dir(pathstn)
	if(stnId == "SG-003E" || stnId == "SG-003S" || stnId == "SG-004S" || stnId == "SG-724S" || stnId == "SG-007E" || stnId == "SG-007X")
		next
	stnId = substr(stnId,1,6)
	mtId = paste(stnId,"A",sep="")
	for(y in 1 : length(years))
	{
		pathmon = paste(pathstn,years[y],sep="/")
		writePathYr = paste(writePath,years[y],sep="/")
		checkdir(writePathYr)
		mons = dir(pathmon)
		for(z in 1 : length(mons))
		{
			print(paste("Doing",stnId,mons[z]))
			pathdays = paste(pathmon,mons[z],sep="/")
			writePathMon = paste(writePathYr,mons[z],sep="/")
			checkdir(writePathMon)
			days = dir(pathdays)
			mtIdBreaks = c(length(days))
			if(!(grepl("txt",days[1])))
			{
				stnsIn = days
				days = c()
				idxdays = 0
				for( m in 1 : length(stnsIn))
				{
					pathInt = paste(pathdays,stnsIn[m],sep="/")
					daysAc = dir(pathInt)
					if(length(daysAc))
					{
						for(n in 1 : length(daysAc))
						{
							idxdays = idxdays + 1
							days[idxdays] = paste(stnsIn[m],daysAc[n],sep="/")
						}
					}
					mtId[m] = paste(stnId,toupper(letters[m]),sep="")
					mtIdBreaks[m] = idxdays
				}
			}
			idxMtId = 1
			for(t in 1:length(days))
			{
				if(!grepl("201",days[t]))
					next
				if(t > mtIdBreaks[idxMtId])
					idxMtId = idxMtId + 1
				writePathFinal = paste(writePathMon,"/EOD_Meter_Reading_SG_",substr(days[t],nchar(days[t])-13,nchar(days[t])),sep="")
				readData = read.table(paste(pathdays,days[t],sep="/"),header=T,sep="\t",stringsAsFactors=F)
				cols = fetchCols(stnId)
				{
				if(file.exists(writePathFinal))
				{
					dataCentral = read.table(writePathFinal,header=T,sep="\t",stringsAsFactors=F)
					stnCentral = as.character(dataCentral[,2])
					mtch = match(mtId[idxMtId],stnCentral)
					if(!is.finite(mtch))
					{
						mtch =  nrow(dataCentral) + 1
					}
					df = data.frame(st = stnId,mt=mtId[idxMtId],lt = as.character(readData[1,cols[1]]),lr=as.character(as.numeric(readData[1,cols[2]])),RInv=as.character(round(as.numeric(readData[1,cols[2]]),0)),stringsAsFactors=F)
					dataCentral[mtch,] = df[1,]
				}
				else
				{
					dataCentral = data.frame(Operation = as.character(stnId),MeterRef = mtId, LastTime = as.character(readData[1,cols[1]]),LastRead = as.character(as.numeric(readData[1,cols[2]])),
					ReadingInv=as.character(round(as.numeric(readData[1,cols[2]]),0)),stringsAsFactors=F)
				}
				}
				dataCentral = dataCentral[order(as.character(dataCentral[,1])),]
				write.table(dataCentral,file=writePathFinal,row.names=F,col.names=T,append=F,sep="\t")
			}
		}
	}
}
Sys.sleep(7200)
}
