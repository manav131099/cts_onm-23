args<-commandArgs(TRUE)
code = paste("[", args[1], "]", sep="")
prbudget = as.numeric(args[2])
rate = as.numeric(args[3])
coddate =args[4]
if(coddate!='TBC'){
  coddate=as.Date(args[4])
}
date = args[5]
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
library(ggplot2)
library(zoo)
library(grid)
library(lubridate)
library(RODBC)
checkdir(paste("/home/admin/Graphs/Graph_Extract", substr(code, 2, 7), sep="/"))
checkdir(paste("/home/admin/Graphs/Graph_Output", substr(code, 2, 7), sep="/"))
station_name = substr(code, 2, 7)
RODBC_connection <- odbcDriverConnect('driver={ODBC Driver 13 for SQL Server};server=cleantechsolar.database.windows.net;database=Cleantech Meter Readings;uid=RohanKN;pwd=R@h@nKN1')
query = sprintf("SELECT Station_Id FROM Stations WHERE Station_Name ='%s'",station_name)
dt <- sqlQuery(channel=RODBC_connection, query = query)
sid = dt[,1]
query = paste("SELECT Meter_id,Capacity FROM Meters WHERE Station_Id = ",sid,sep="")
dm<- sqlQuery(channel=RODBC_connection, query = query)
query = paste("SELECT Date,GHI,PR,Meter_Id FROM Stations_Data WHERE Station_Id = ",sid,sep="")
dff <- sqlQuery(channel=RODBC_connection, query = query)
odbcClose(RODBC_connection)

yr_cnt=time_length(difftime(as.Date(date), coddate), "years")

dff_M1=dff[dff$Meter_Id == 48,]
dff_M2=dff[dff$Meter_Id == 49,]
dff_M3=dff[dff$Meter_Id == 50,]

if(length(unique(dff[["Meter_Id"]]))>1){
for(x in 1 :nrow(dm))
{
  dff[dff$Meter_Id == dm[x,1],3]=(dff[dff$Meter_Id == dm[x,1],3]*dm[x,2])
}
prs=setNames(aggregate(dff$PR, by=list(Date=dff$Date), FUN=sum),c("Date", "PR"))
prs[,2]=prs[,2]/sum(dm[,2])
ghi=setNames(aggregate(dff$GHI, by=list(Date=dff$Date), FUN=mean),c("Date", "GHI"))
dff<- merge(ghi,prs,by="Date")
}

df_list= list(dff,dff_M1,dff_M2,dff_M3)
m_names=c('Full Site','M1','M2','M3')

cnt=0
for(m in df_list){
  coddate=as.Date("2017-03-01")
  m = m[(as.Date(m$Date)>= coddate),]
  cnt=cnt+1

  pathwritetxt <- paste("/home/admin/Graphs/Graph_Extract", substr(code, 2, 7), paste("[", substr(code, 2, 7), "] Graph ", date, " - PR Evolution - ",m_names[cnt],".txt", sep=""), sep="/")
  graph <- paste("/home/admin/Graphs/Graph_Output", substr(code, 2, 7), paste("[", substr(code, 2, 7), "] Graph ", date, " - PR Evolution",' - ',m_names[cnt],".pdf", sep=""), sep="/")
  for(x in 1 :nrow(m))
  {
    if(is.finite(m[x,3]) && (as.numeric(m[x,3]) < 10))# || (as.numeric(m[x,4]) < 50)
      m[x,3] = NA
  }
  m = m[complete.cases(as.numeric(m[,3])),]
  m = m[(m[,3] <= 95),]
  m = m[(m[,3] >= 0),]
  m = m[complete.cases(as.numeric(m[,2])),]
  m = m[(as.Date(m$Date)<= date),]
  m = unique(m)

  #moving average 7d/30d
  zoo.PR <- zoo(m[,3], as.Date(m$Date))
  if(nrow(m)<120){
  ma1 <- rollapplyr(zoo.PR, list(-(6:1)), mean, fill = NA, na.rm = T)
  }else{
  ma1 <- rollapplyr(zoo.PR, list(-(29:1)), mean, fill = NA, na.rm = T)
  }
  m$ambPR.av = coredata(ma1)
  row.names(m) <- NULL

  firstdate <- as.Date(m[1,1])
  lastdate <- as.Date(m[length(m[,1]),1])
  PRUse = as.numeric(m$PR)
  PRUse = PRUse[complete.cases(PRUse)]
  last7 = round(mean(tail(PRUse,7)),1)
  last30 = round(mean(tail(PRUse,30)),1)
  last60 = round(mean(tail(PRUse,60)),1)
  last90 = round(mean(tail(PRUse,90)),1)
  last365 = round(mean(tail(PRUse,365)),1)
  lastlt = round(mean(PRUse),1)
  rightIdx = nrow(m) * 0.75
  text_pos = round(nrow(m) * 0.40, 0)-nrow(m)* yr_cnt *0.01
  seg_start=nrow(m)* 0.35-nrow(m)* yr_cnt *0.01
  seg_end=nrow(m)* 0.38-nrow(m)* yr_cnt *0.01


  #data sorting for visualisation
  m$colour[m[,2] < 2] <- '< 2'
  m$colour[m[,2] >= 2 & m[,2] <= 4] <- '2 ~ 4'
  m$colour[m[,2] >= 4 & m[,2] <= 6] <- '4 ~ 6'
  m$colour[m[,2] > 6] <- '> 6'
  m$colour = factor(m$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
  titlesettings <- theme(plot.title = element_text(face = "bold", size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text(face = "bold", size = 12, lineheight = 0.9, hjust = 0.5))

  p <- ggplot() + theme_bw()

  #30-d / 7-d average line

  p <- p + geom_point(data=m, aes(x=as.Date(Date), y = PR, shape = as.factor(18), colour = colour), size = 2) + guides(shape = FALSE)
  p <- p + ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
  if(nrow(m)<120){
      p <- p + scale_x_date(expand=c(0,0),date_breaks = "7 days", date_labels = "%d-%b", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
      p <- p + annotate("segment", x = as.Date(m[seg_start,1]), xend = as.Date(m[seg_end,1]), y=45, yend=45, colour="red", size=1.5)
      p <- p + annotate('text', label = "7-d moving average of PR", y = 45, x = as.Date(m[text_pos,1]), colour = "red",fontface =2,hjust = 0)
  }else{
      p <- p + scale_x_date(expand=c(0,0),date_breaks = "3 months", date_labels = "%b/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
      p <- p + annotate("segment", x = as.Date(m[seg_start,1]), xend = as.Date(m[seg_end,1]), y=45, yend=45, colour="red", size=1.5)
      p <- p + annotate('text', label = "30-d moving average of PR", y = 45, x = as.Date(m[text_pos,1]), colour = "red",fontface =2,hjust = 0)
  }
  p <- p + theme(plot.margin = margin(10, 20, 0, 10),axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97))                     #LEGNED AT RHS
  p <- p + titlesettings + ggtitle(paste0('Performance Ratio Evolution - ', substr(code, 2, 7),' - ',m_names[cnt],sep=''), subtitle = paste0('From ', firstdate, ' to ', lastdate))
  p <- p + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
  p <- p + scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) 
  p <- p + scale_shape_manual(values = 18)
  p <- p + theme(legend.box = "horizontal", legend.direction="horizontal") #legend.position = "bottom"

  coddate_og=coddate

  if(inherits(coddate, c("Date", "POSIXt"))){
  }else{
    coddate=firstdate
  }
  #Creating steps using budget PR
  prthresh = c(prbudget,prbudget)
  dates = c(firstdate,coddate)
  temp=prbudget
  while(coddate<as.Date(date) && (as.Date(date)-coddate)>365){#
      coddate= coddate %m+% months(12)
      temp=temp-(temp*rate)
      prthresh=c(prthresh,temp)
      dates=c(dates,coddate)
  }
  prthresh=c(prthresh,temp)
  dates=c(dates,lastdate)
  p <- p +geom_step(mapping=aes(x=dates,y=prthresh),size=1,color='darkgreen')
  m$Date=as.Date(m$Date, format = "%Y-%m-%d")
  total=0
  text="Target Budget Yield Performance Ratio ["
  for(i in 2:(length(dates)-1)){#n-1 because we are taking number of years between. 
      if(i==(length(prthresh)-1)){
      text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%]", sep="")
      }else{
      text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%,", sep="")
      }
  }
  for(i in 1:(length(dates)-1)){
      if(i==(length(dates)-1)){
        total=total+sum(m[m$Date >= dates[i] & m$Date <=dates[i+1],'PR']>prthresh[i])
      }else{
        total=total+sum(m[m$Date >= dates[i] & m$Date < dates[i+1],'PR']>prthresh[i])
      }
  }

  #Adding Legend + Stats
  p <- p + guides(colour = guide_legend(override.aes = list(shape = 18)))
  p <- p + geom_line(data=m, aes(x=as.Date(Date), y=ambPR.av), color="red", size=1.5)
  p <- p + annotate("segment", x = as.Date(m[seg_start,1]), xend = as.Date(m[seg_end,1]), y=50, yend=50, colour="darkgreen", size=1.5)
  p <- p + annotate('text', label = text, y = 50, x = as.Date(m[text_pos,1]), colour = "darkgreen",fontface =2,hjust = 0)
  p <- p + annotate('text', label = paste("Points above Target Budget PR = ",total,'/',nrow(m),' = ',round((total/nrow(m))*100,1),'%',sep=''), y = 40, x = as.Date(m[text_pos,1]), colour = "black",fontface =2,hjust = 0)
  p <- p + annotate('text', label = paste('Commissioning Date: ',coddate_og,sep=" "), y = 35, x = as.Date(m[text_pos,1]), colour = "black",fontface =2,hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 7-d:", last7, "%"), y=30, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 30-d:", last30, "%"), y=25, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 60-d:", last60, "%"), y=20, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 90-d:", last90, "%"), y=15, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 365-d:", last365,"%"), y=10, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=5, x=as.Date(m[rightIdx,1]), colour="black", hjust = 0,fontface=2)
  p <- p + annotate("segment", x = as.Date(m[nrow(m),1]), xend = as.Date(m[nrow(m),1]), y=0, yend=100, colour="deeppink4", size=1.5, alpha=0.5)

  ggsave(paste0(graph), p, width = 11.5, height=8)
  write.table(m, na = "", pathwritetxt, row.names = FALSE, sep ="\t")

}
