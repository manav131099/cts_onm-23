rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .001*1000
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

getPRData = function(df,sleep)
{
	PR1=PR2=gsi=NA
	date = as.character(df[,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Second Gen/[MY-006S]/',yr,"/",mon,"/[MY-006S] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if((!file.exists(path)) && sleep)
	{
		Sys.sleep(60*60*2)
	}
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t")
	dsp1 = as.numeric(df[1,6])
	dsp2 = as.numeric(df[1,9])
	gsi = as.numeric(dataread[1,4])
	PR1 = format(round((dsp1 *100/ gsi),1),nsmall=1)
	PR2 = format(round((dsp2 *100/ gsi),1),nsmall=1)
	}
	array2 = c(PR1,PR2,gsi)
	return(array2)
}

secondGenData = function(filepath,writefilepath,needsleep)
{
	TIMESTAMPSALARM <<- NULL
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
  idx1en = 48
	idx2en = 19
	if(nrow(dataread) < 1 || ncol(dataread)<39) 
	{
	  print('Err in file')
		return(NULL)
	}
	dataread2 = dataread
	datewrite = substr(as.character(dataread[1,1]),1,10)
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idx1en])),]
	lastt = NA
	lastr = NA
	{
	if(nrow(dataread) < 1)
	{
	 Eac2 = 0
	}
	else{
	Eac2 = format(round((as.numeric(dataread[nrow(dataread),idx1en]) - as.numeric(dataread[1,idx1en]))/1000,1),nsmall=1)
	lastt = as.character(dataread[nrow(dataread),1])
	lastr = as.character(as.numeric(dataread[nrow(dataread),idx1en])/1000)
	}
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idx2en])),]
  {
	if(nrow(dataread) < 1){
	 Eac1 = 0}
	else{
	Eac1 = format(round(sum(as.numeric(dataread[,idx2en]))/-60000,1),nsmall=1)
	}
	}
	dataread = dataread2
	DSPY = round(as.numeric(Eac1)/345.8,2)
	DSPY2 = round(as.numeric(Eac2)/345.8,2)
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idx2en])),]
  missingfactor = 480 - nrow(dataread2)
  dataread2 = dataread2[abs(as.numeric(dataread2[,idx2en])) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
			TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
  df = data.frame(Date = datewrite, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield1 = DSPY,LastTime = lastt,LastRead=lastr,DailySpecYield2=DSPY2,stringsAsFactors=F)
  vals = getPRData(df,needsleep)
  df = data.frame(Date = datewrite, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield = DSPY,
									LastTime = lastt,LastRead=lastr,DailySpecYield2=DSPY2,
									PR1=vals[1],PR2=vals[2],GSI=vals[3],stringsAsFactors=F)

	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F)
  Eac1T = as.numeric(dataread1[,2])
  Eac2T = as.numeric(dataread1[,3])
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),EacTotMethod1 = Eac1T,EacTotMethod2=Eac2T,DailySpecYield1 = as.character(dataread1[,6]),
	LastTime = as.character(dataread1[,7]),LastRead = as.character(dataread1[,8]),DailySpecYield2=as.character(dataread1[,9]),PR1=as.character(dataread1[,10]),
	PR2=as.character(dataread1[,11]),GSI=as.character(dataread1[,12]),stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
  return(c(as.numeric(Eac1T),as.numeric(Eac2T)))
}

